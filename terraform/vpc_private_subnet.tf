resource "aws_vpc" "this" {
  cidr_block                       = var.vpc_cidr
  enable_dns_hostnames             = var.enable_dns_hostnames
  enable_dns_support               = var.enable_dns_support
  assign_generated_ipv6_cidr_block = var.assign_generated_ipv6_cidr_block
  tags = merge(
    module.required_tags.tags,
    {
      "Name"                                         = var.name
      "mnfgroup:name"                                = var.name
      "kubernetes.io/cluster/${terraform.workspace}" = "shared"
    },
  )
}

resource "aws_route_table" "private" {
  count  = 1
  vpc_id = aws_vpc.this.id
  tags = merge(
    module.required_tags.tags,
    {
      "Name"          = "${var.name}-${var.private_subnet_suffix}-${var.azs}-${count.index}"
      "mnfgroup:name" = "${var.name}-${var.private_subnet_suffix}-${var.azs}-${count.index}"
    },
  )
  lifecycle {
    # When attaching VPN gateways it is common to define aws_vpn_gateway_route_propagation
    # resources that manipulate the attributes of the routing table (typically for the private subnets)
    ignore_changes = [propagating_vgws]
  }
}


resource "aws_route" "shared_vpc_private" {
  count                  = 1
  route_table_id         = element(aws_route_table.private.*.id, count.index)
  destination_cidr_block = var.shared_vpc_cidr
  #transit_gateway_id     = data.terraform_remote_state.transit_gateway.outputs.transit_gateway_id
  gateway_id = aws_internet_gateway.batfish-igw.id
  timeouts {
    create = "5m"
  }
}

resource "aws_subnet" "this" {

  vpc_id = aws_vpc.this.id

  cidr_block        = var.private_subnets
  availability_zone = var.azs

  tags = merge(
    {
      "Name" = "${var.name}-${var.private_subnet_suffix}-${var.azs}"
    },
    module.required_tags.tags
  )
}

resource "aws_route_table_association" "this" {
  count          = 1
  subnet_id      = element(aws_subnet.this.*.id, count.index)
  route_table_id = element(aws_route_table.private.*.id, count.index)
}



########### Added IGW and NAT Gateway ###########

### Creating Internet Gateway

resource "aws_internet_gateway" "batfish-igw" {
  vpc_id = aws_vpc.this.id

  tags = merge(
    module.required_tags.tags,
    {
      "Name"          = "batfish-igw",
      "mnfgroup:name" = "batfish-dev-batfish-igw"
    },
  )
}

### Creating NAT Gateway

resource "aws_eip" "batfish-eip-nat" {
  vpc = true
}

resource "aws_nat_gateway" "batfish-nat-gateway" {
  allocation_id = aws_eip.batfish-eip-nat.id
  subnet_id     = aws_subnet.this.id

  tags = merge(
    module.required_tags.tags,
    {
      "Name"          = "batfish-nat-gw",
      "mnfgroup:name" = "batfish-dev-batfish-nat-gw"
    },
  )
}

### IGW Route

resource "aws_route" "batfish-igw-route" {
  route_table_id         = element(aws_route_table.private.*.id, 1)
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.batfish-igw.id
  timeouts {
    create = "5m"
  }
}


### NAT Route

resource "aws_route" "batfish-nat-route" {
  route_table_id         = element(aws_route_table.private.*.id, 0)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.batfish-nat-gateway.id
  timeouts {
    create = "5m"
  }
}
