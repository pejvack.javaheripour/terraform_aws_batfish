variable "aws_region" {
  type = string
}
variable "aws_profile" {
  type = string
}
variable "ssh_icmp_ingress_rules" {
  type = list(any)
}
variable "bf_instance_type" {
  type = string
}
variable "my_key_pair" {
  type = string
}
variable "bf_image_id" {
  type = string
}
variable "bf_env_name" {
  type = string
}
variable "gr_env_name" {
  type = string
}
variable "enable_dns_support" {
  description = "Should be true to enable DNS support in the VPC"
  default     = true
}
variable "enable_dns_hostnames" {
  description = "Should be true to enable DNS hostnames in the VPC"
  default     = true
}
variable "vpc_cidr" {
  description = "The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overridden"
  default     = "0.0.0.0/0"
}
variable "assign_generated_ipv6_cidr_block" {
  description = "Requests an Amazon-provided IPv6 CIDR block with a /56 prefix length for the VPC. You cannot specify the range of IP addresses, or the size of the CIDR block"
  default     = false
}
variable "name" {
  description = "Name to be used on all the resources as identifier"
  default     = ""
}
variable "azs" {
  description = "A list of availability zones in the region"
  default     = []
}
variable "private_subnet_suffix" {
  description = "Suffix to append to private subnets name"
  default     = "private"
}
variable "shared_vpc_cidr" {
  description = "the shared VPC cidr"
  type        = string
  default     = "10.226.0.0/16"
}
variable "private_subnets" {
  description = "A list of private subnets inside the VPC"
  default     = []
}