resource "aws_iam_policy" "tr_bf_iam_policy" {
  name        = "tr-bf-iam-policy"
  path        = "/"
  description = "Allow BF instance to copy across the batfish content into S3"

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "VisualEditor0",
        "Effect" : "Allow",
        "Action" : [
          "s3:PutObject",
          "s3:GetObject",
          "s3:ListBucket",
          "s3:DeleteObject"
        ],
        "Resource" : [
          "arn:aws:s3:::*/*",
          "arn:aws:s3:::tr-bf-bucket"
        ]
      }
    ]
  })
  tags = {
    env = "tr-bf"
  }
}

resource "aws_iam_role" "tr_bf_iam_role" {
  name = "tr-bf-iam-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy_attachment" "tr_bf_iam_role_policy_attachment" {
  role       = aws_iam_role.tr_bf_iam_role.name
  policy_arn = aws_iam_policy.tr_bf_iam_policy.arn
}

resource "aws_iam_instance_profile" "tr_bf_iam_instance_profile" {
  name = "tr-bf-iam-instance-profile"
  role = aws_iam_role.tr_bf_iam_role.name
}