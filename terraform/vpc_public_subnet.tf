resource "aws_vpc" "tr_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "${var.bf_env_name} VPC"
  }
}

resource "aws_subnet" "tr_public_subnet" {
  vpc_id = aws_vpc.tr_vpc.id
  #cidr_block        = "10.0.0.0/24"
  cidr_block        = cidrsubnet(aws_vpc.tr_vpc.cidr_block, 8, 53)
  availability_zone = "ap-southeast-2a"

  tags = {
    Name = "${var.bf_env_name} Public Subnet"
  }
}

resource "aws_internet_gateway" "tr_vpc_igw" {
  vpc_id = aws_vpc.tr_vpc.id

  tags = {
    Name = "${var.bf_env_name} Internet_Gateway"
  }
}

resource "aws_route_table" "tr_rt_ap_southeast_2a_public" {
  vpc_id = aws_vpc.tr_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.tr_vpc_igw.id
  }

  tags = {
    Name = "${var.bf_env_name} Public Subnet Route Table."
  }
}

resource "aws_route_table_association" "tr_vpc_us_east_1a_public" {
  subnet_id      = aws_subnet.tr_public_subnet.id
  route_table_id = aws_route_table.tr_rt_ap_southeast_2a_public.id
}