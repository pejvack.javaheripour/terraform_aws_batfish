#!/bin/bash

# Adding EPEL & IUS Reposifity:
sudo yum -y install epel-release
sudo rpm -ivh https://repo.ius.io/ius-release-el7.rpm

# Installing Podman:
sudo yum -y install podman

# Installing PIP3:
sudo yum install python3-pip -y

# Installing Pybatfish:
sudo python3 -m pip install --upgrade pybatfish
sudo python3 -m pip install ipython
sudo python3 -m pip install Jinja2

# Installing AWS CLI:
sudo curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
sudo unzip awscliv2.zip
sudo ./aws/install

# Pulling Batfish image in Podman:
sudo podman pull batfish/allinone

# Creating a directory in host to be mounted to batfish container:
sudo mkdir /root/batfish-data
sudo touch /root/batfish-data/touch_created

# Creating log directory for cron:
sudo mkdir -p /var/log/backups/
sudo touch /var/log/backups/s3-batfish-data-sync.log

# copy batfish data (backups) from S3
sudo /usr/bin/aws s3 cp s3://tr-bf-bucket/batfish-data/ /root/batfish-data/ --recursive >> /var/log/backups/s3-batfish-data-sync.log 2>&1 

# creating a cron to create backup from batfish data directory.
sudo cat <<EOF > /etc/cron.hourly/s3-batfish-data-backup
#!/bin/bash

# S3 Backup cron daily

set -e

echo "Commencing S3 /data backup batfish-data" >> /var/log/backups/s3-batfish-data-sync.log
sudo /usr/bin/aws s3 sync /root/batfish-data/ s3://tr-bf-bucket/batfish-data/ --exclude '*.log*' >> /var/log/backups/s3-batfish-data-sync.log 2>&1
echo "Completed S3 /data backup batfish-data" >> /var/log/backups/s3-batfish-data-sync.log
EOF

sudo chown root:root /etc/cron.hourly/s3-batfish-data-backup
sudo chmod 755 /etc/cron.hourly/s3-batfish-data-backup


# Creating Batfish container:
sudo podman run -d --name batfish --privileged --mount type=bind,source=/root/batfish-data,target=/data -p 8888:8888 -p 9997:9997 -p 9996:9996 batfish/allinone