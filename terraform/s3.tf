resource "aws_s3_bucket" "tr_bf_s3_bucket" {
  bucket        = "tr-bf-bucket"
  force_destroy = true
}

resource "aws_s3_bucket_object" "batfish-data-folder" {
  bucket = aws_s3_bucket.tr_bf_s3_bucket.id
  key    = "batfish-data/"
  source = "/dev/null"
}

resource "aws_s3_bucket_public_access_block" "tr_bf_s3_bucket_public_access_block" {
  bucket = aws_s3_bucket.tr_bf_s3_bucket.id

  block_public_acls   = true
  block_public_policy = true
  ignore_public_acls  = true
}