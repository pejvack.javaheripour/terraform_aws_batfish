/*
variable "ssh_icmp_ingress_rules" {
	default = [{
		from_port   = 22
    to_port     = 22
		description = "SSH traffic"
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
	},
	{
		from_port   = 8
    to_port     = 0
		description = "ICMP traffic"
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "icmp"
	}]
}
*/

resource "aws_security_group" "tr_allow_ssh_sg" {
  name        = "tr-allow-ssh-sg"
  description = "Allow SSH and ICMP inbound connections and created by tr"
  vpc_id      = aws_vpc.tr_vpc.id

  dynamic "ingress" {
    for_each = var.ssh_icmp_ingress_rules

    content {
      description = ingress.value.description
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }

  /*
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
*/

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.bf_env_name} - allow ssh and ICMP"
  }
}