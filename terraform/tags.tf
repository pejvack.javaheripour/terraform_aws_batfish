module "required_tags" {
  source      = "git::ssh://git@gitlab.com/mnf-group/ecosystem/terraform/modules/required_tags.git?ref=1.4.0"
  contact     = "systems-cloud@mnfgroup.limited"
  project     = "batfish"
  team        = "systems-engineering"
  environment = terraform.workspace
  name        = "batfish"
  category    = "network"
  gitlab-repo = "batfish-networking"
  region      = "ap-southeast-2"
}