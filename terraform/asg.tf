data "template_file" "tr_user_data_bf" {
  template = file("user_data_bf.sh")
}

#data "template_file" "tr_user_data_gr" {
#  template = file("user_data_gr.sh")
#}

##################################################################################
#Batfish Server
##################################################################################
resource "aws_launch_template" "tr_bf_launch_template" {
  name                    = "${var.bf_env_name}-lunch-template"
  disable_api_termination = true
  iam_instance_profile {
    name = aws_iam_instance_profile.tr_bf_iam_instance_profile.name
  }
  image_id      = var.bf_image_id
  instance_type = var.bf_instance_type
  key_name      = var.my_key_pair
  network_interfaces {
    security_groups             = [aws_security_group.tr_allow_ssh_sg.id]
    delete_on_termination       = true
    associate_public_ip_address = true
  }
  user_data = base64encode("${data.template_file.tr_user_data_bf.rendered}")
  tag_specifications {
    resource_type = "instance"
    tags = merge(
      module.required_tags.tags,
      {
        "Name"          = "batfish-server",
        "mnfgroup:name" = "batfish-batfish-server"
      }
    )
  }
  /*
  tag_specifications {
    resource_type = "volume"
    tags = merge(
      module.required_tags.tags,
      {
        "Name"          = "batfish-server",
        "mnfgroup:name" = "batfish-batfish-server"
      }
    )
  }
  */
}

resource "aws_autoscaling_group" "tr_bf_asg" {
  name                = "${var.bf_env_name}-asg"
  desired_capacity    = 1
  max_size            = 1
  min_size            = 1
  health_check_type   = "EC2"
  vpc_zone_identifier = [aws_subnet.tr_public_subnet.id]

  launch_template {
    id      = aws_launch_template.tr_bf_launch_template.id
    version = "$Latest"
  }

  tags = concat(
    [
      {
        "key"                 = "mnfgroup:category"
        "value"               = "batfish"
        "propagate_at_launch" = false
      },
      {
        "key"                 = "Name"
        "value"               = "${terraform.workspace}-batfish"
        "propagate_at_launch" = false
      },
      {
        "key"                 = "mnfgroup:team"
        "value"               = "systems-engineering"
        "propagate_at_launch" = false
      },
      {
        "key"                 = "mnfgroup:contact"
        "value"               = "systems-cloud@mnfgroup.limited"
        "propagate_at_launch" = false
      },
      {
        "key"                 = "mnfgroup:project"
        "value"               = "batfish-${terraform.workspace}"
        "propagate_at_launch" = false
      },
      {
        "key"                 = "mnfgroup:name"
        "value"               = "${terraform.workspace}-batfish"
        "propagate_at_launch" = false
      },
      {
        "key"                 = "mnfgroup:environment"
        "value"               = terraform.workspace
        "propagate_at_launch" = false
      },
      {
        "key"                 = "mnfgroup:tagging-version"
        "value"               = "3.0"
        "propagate_at_launch" = false
      },
      {
        "key"                 = "mnfgroup:provisioner"
        "value"               = "terraform"
        "propagate_at_launch" = false
      },
    ],
  )
}

/*
##################################################################################
#Gitlab Runner Server
##################################################################################
resource "aws_launch_template" "tr_gr_launch_template" {
  name                    = "${var.gr_env_name}-lunch-template"
  disable_api_termination = true
  
  #iam_instance_profile {
  #  name = aws_iam_instance_profile.tr_bf_iam_instance_profile.name
  #}
  
  image_id      = var.bf_image_id
  instance_type = var.bf_instance_type
  key_name      = var.my_key_pair
  network_interfaces {
    security_groups             = [aws_security_group.tr_allow_ssh_sg.id]
    delete_on_termination       = true
    associate_public_ip_address = true
  }

  user_data = base64encode("${data.template_file.tr_user_data_gr.rendered}")
}

resource "aws_autoscaling_group" "tr_gr_asg" {
  name                = "${var.gr_env_name}-asg"
  desired_capacity    = 1
  max_size            = 1
  min_size            = 1
  health_check_type   = "EC2"
  vpc_zone_identifier = [aws_subnet.tr_public_subnet.id]

  launch_template {
    id      = aws_launch_template.tr_gr_launch_template.id
    version = "$Latest"
  }
}
*/